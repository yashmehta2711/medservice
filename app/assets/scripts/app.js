import $ from 'jquery';
import "lazysizes";
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScoll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";


//Handles Mobile Menu/Header
let mobileMenu = new MobileMenu();

//Handles Reveal On Scroll
new RevealOnScoll($("#our-beginning"));
new RevealOnScoll($("#departments"));
new RevealOnScoll($("#counters"));
new RevealOnScoll($("#testimonials"));

//Adding smooth scroll functionality to our header links
new SmoothScroll();

//Adding Active Links status functionality to our header links
new ActiveLinks();

//
new Modal(); 

if(module.hot){ //for injecting CSS and JS on refresh
    module.hot.accept();
} 
//alert("Hello World");
console.log("This is console!");
console.log("Another Line!");